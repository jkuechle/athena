################################################################################
# Package: OverlayTests
################################################################################

# Declare the package name:
atlas_subdir( OverlayTests )

# Declare the package's dependencies:
atlas_depends_on_subdirs( PRIVATE
                          TestPolicy )
